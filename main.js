let inputFrase = document.getElementById('inputFrase');
let btnInvio = document.getElementById('btnInvio');
let btnDecodifica = document.getElementById('btnDecodifica');
let fraseInseritaDOM = document.getElementById('fraseInserita');
let fraseCodificataDOM = document.getElementById('fraseCodificata');
let fraseDecodificataDOM = document.getElementById('fraseDecodificata');

let testoInserito = document.createElement('h3');
let testoCodificato = document.createElement('h3');
let testoDecodificato = document.createElement('h3');

function codificaCesare(frase, shift) {
    let fraseInserita = frase.value;
    let fraseSenzaSpazi = fraseInserita.trim();
    let fraseCodificata = '';

    for (let i = 0; i < fraseInserita.length; i++) {

        if (fraseInserita.charAt(i) == ' ') { fraseCodificata += fraseInserita.charAt(i) }
        else {
            let carattereAscii = fraseSenzaSpazi.charCodeAt(i);
            let lettera = carattereAscii + shift;
            if (lettera > 122) {
                lettera -= 26;
            }

            fraseCodificata += String.fromCharCode(lettera);
        }

        testoCodificato.innerHTML = fraseCodificata;
        fraseCodificataDOM.appendChild(testoCodificato)
        // fraseCodificataDOM.innerHTML = 'Frase Codificata: ' + fraseCodificata;
    }
    return fraseCodificata;
}

function decodificaFrase(frase, shift) {
    let fraseInserita = frase;
    let fraseSenzaSpazi = fraseInserita.trim();
    let fraseDecodificata = '';

    for (let i = 0; i < fraseInserita.length; i++) {

        if (fraseInserita.charAt(i) == ' ') { fraseDecodificata += fraseInserita.charAt(i) }
        else {
            let carattereAscii = fraseSenzaSpazi.charCodeAt(i);
            let lettera = carattereAscii - shift;
            if (lettera < 97) {
                lettera += 26;
            }
            console.log(lettera);

            fraseDecodificata += String.fromCharCode(lettera);
        }

        testoDecodificato.innerHTML = fraseDecodificata;
        fraseDecodificataDOM.appendChild(testoDecodificato)
        //fraseCodificataDOM.innerHTML = 'Frase Decodificata: ' + fraseDecodificata;
    }
    return fraseDecodificata;
}

inputFrase.addEventListener('keyup', e => {
    e.preventDefault();

    testoInserito.innerHTML = inputFrase.value;
    fraseInseritaDOM.appendChild(testoInserito);

});

btnDecodifica.addEventListener('click', e => {
    e.preventDefault();

    decodificaFrase(codificaCesare(inputFrase, 2), 2);
    console.log('Testo Decodificato: ' +testoDecodificato.innerHTML);
})

btnInvio.addEventListener('click', e => {
    e.preventDefault();

    codificaCesare(inputFrase, 2);
    console.log('Frase Inserita: '+testoInserito.innerHTML);
    console.log('Testo Codificato: ' +testoCodificato.innerHTML);
})